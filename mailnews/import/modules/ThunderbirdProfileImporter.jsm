/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

const EXPORTED_SYMBOLS = ["ThunderbirdProfileImporter"];

var { setTimeout } = ChromeUtils.import("resource://gre/modules/Timer.jsm");
var { Services } = ChromeUtils.import("resource://gre/modules/Services.jsm");
var { MailServices } = ChromeUtils.import(
  "resource:///modules/MailServices.jsm"
);

/**
 * An object to represent a source profile to import from.
 * @typedef {Object} SourceProfile
 * @property {string} name - The profile name.
 * @property {nsIFile} dir - The profile location.
 *
 * An object to represent items to import.
 * @typedef {Object} ImportItems
 * @property {boolean} accounts - Whether to import accounts and settings.
 * @property {boolean} addressBooks - Whether to import address books.
 * @property {boolean} calendars - Whether to import calendars.
 * @property {boolean} mailMessages - Whether to import mail messages.
 *
 * A pref is represented as [type, name, value].
 * @typedef {["Bool"|"Char"|"Int", string, number|string|boolean]} PrefItem
 *
 * A map from source smtp server key to target smtp server key.
 * @typedef {Map<string, string>} SmtpServerKeyMap
 *
 * A map from source identity key to target identity key.
 * @typedef {Map<string, string>} IdentityKeyMap
 *
 * A map from source IM account key to target IM account key.
 * @typedef {Map<string, string>} IMAccountKeyMap
 *
 * A map from source incoming server key to target incoming server key.
 * @typedef {Map<string, string>} IncomingServerKeyMap
 */

// Pref branches that need special handling.
const ACCOUNT_MANAGER = "mail.accountmanager.";
const MAIL_IDENTITY = "mail.identity.";
const MAIL_SERVER = "mail.server.";
const MAIL_ACCOUNT = "mail.account.";
const IM_ACCOUNT = "messenger.account.";
const MAIL_SMTP = "mail.smtp.";
const SMTP_SERVER = "mail.smtpserver.";
const ADDRESS_BOOK = "ldap_2.servers.";
const LDAP_AUTO_COMPLETE = "ldap_2.autoComplete.";
const CALENDAR = "calendar.registry.";

// Prefs (branches) that we do not want to copy directly.
const IGNORE_PREFS = [
  "app.update.",
  "browser.",
  "calendar.list.sortOrder",
  "calendar.timezone",
  "devtools.",
  "extensions.",
  "mail.cloud_files.accounts.",
  "mail.newsrc_root",
  "mail.root.",
  "mail.smtpservers",
  "messenger.accounts",
  "print.",
  "services.",
  "toolkit.telemetry.",
];

/**
 * A class that can import things from another thunderbird profile dir into the
 * current profile.
 */
class ThunderbirdProfileImporter {
  useFilePicker = true;

  /** @type ImportItems */
  supportedItems = {
    accounts: true,
    addressBooks: true,
    calendars: true,
    mailMessages: true,
  };

  /** When importing from a zip file, ignoring these folders. */
  IGNORE_DIRS = [
    "chrome_debugger_profile",
    "crashes",
    "datareporting",
    "extensions",
    "extension-store",
    "logs",
    "minidumps",
    "saved-telemetry-pings",
    "security_state",
    "storage",
    "xulstore",
  ];

  /**
   * Callback for progress updates.
   * @param {number} current - Current imported items count.
   * @param {number} total - Total items count.
   */
  onProgress = () => {};

  _logger = console.createInstance({
    prefix: "mail.import",
    maxLogLevel: "Warn",
    maxLogLevelPref: "mail.import.loglevel",
  });

  /**
   * @type {SourceProfile[]} - Other thunderbird profiles found on this machine.
   */
  get sourceProfiles() {
    let profileService = Cc[
      "@mozilla.org/toolkit/profile-service;1"
    ].getService(Ci.nsIToolkitProfileService);
    let sourceProfiles = [];
    for (let profile of profileService.profiles) {
      if (profile == profileService.currentProfile) {
        continue;
      }
      sourceProfiles.push({
        name: profile.name,
        dir: profile.rootDir,
      });
    }
    return sourceProfiles;
  }

  /**
   * Increase _itemsImportedCount by one, and call onProgress.
   */
  async _updateProgress() {
    this.onProgress(++this._itemsImportedCount, this._itemsTotalCount);
    return new Promise(resolve => setTimeout(resolve));
  }

  /**
   * Actually start importing things to the current profile.
   * @param {nsIFile} sourceProfileDir - The source location to import from.
   * @param {ImportItems} items - The items to import.
   */
  async startImport(sourceProfileDir, items) {
    this._logger.debug(
      `Start importing from ${sourceProfileDir.path}, items=${JSON.stringify(
        items
      )}`
    );

    this._sourceProfileDir = sourceProfileDir;
    this._items = items;
    this._itemsTotalCount = Object.values(items).filter(Boolean).length;
    this._itemsImportedCount = 0;

    if (items.accounts || items.addressBooks || items.calendars) {
      await this._loadPreferences();
    }

    if (this._items.accounts) {
      await this._importServersAndAccounts();
      this._importPasswords();
      this._importOtherPrefs(this._otherPrefs);
      await this._updateProgress();
    }

    if (this._items.addressBooks) {
      this._importAddressBooks(
        this._branchPrefsMap.get(ADDRESS_BOOK),
        this._collectPrefsToObject(this._branchPrefsMap.get(LDAP_AUTO_COMPLETE))
      );
      await this._updateProgress();
    }

    if (this._items.calendars) {
      this._importCalendars(this._branchPrefsMap.get(CALENDAR));
      await this._updateProgress();
    }

    if (!this._items.accounts && this._items.mailMessages) {
      this._importMailMessagesToLocal();
    }

    await this._updateProgress();
  }

  /**
   * Collect interested prefs from this._sourceProfileDir.
   */
  async _loadPreferences() {
    // A Map to collect all prefs in interested pref branches.
    // @type {Map<string, PrefItem[]>}
    this._branchPrefsMap = new Map([
      [ACCOUNT_MANAGER, []],
      [MAIL_IDENTITY, []],
      [MAIL_SERVER, []],
      [MAIL_ACCOUNT, []],
      [IM_ACCOUNT, []],
      [MAIL_SMTP, []],
      [SMTP_SERVER, []],
      [ADDRESS_BOOK, []],
      [LDAP_AUTO_COMPLETE, []],
      [CALENDAR, []],
    ]);
    this._otherPrefs = [];

    let sourcePrefsFile = this._sourceProfileDir.clone();
    sourcePrefsFile.append("prefs.js");
    let sourcePrefsBuffer = await IOUtils.read(sourcePrefsFile.path);

    let savePref = (type, name, value) => {
      for (let [branchName, branchPrefs] of this._branchPrefsMap) {
        if (name.startsWith(branchName)) {
          branchPrefs.push([type, name.slice(branchName.length), value]);
          return;
        }
      }
      if (IGNORE_PREFS.some(ignore => name.startsWith(ignore))) {
        return;
      }
      // Collect all the other prefs.
      this._otherPrefs.push([type, name, value]);
    };

    Services.prefs.parsePrefsFromBuffer(sourcePrefsBuffer, {
      onStringPref: (kind, name, value) => savePref("Char", name, value),
      onIntPref: (kind, name, value) => savePref("Int", name, value),
      onBoolPref: (kind, name, value) => savePref("Bool", name, value),
      onError: msg => {
        throw new Error(msg);
      },
    });
  }

  /**
   * Import all the servers and accounts.
   */
  async _importServersAndAccounts() {
    // Import SMTP servers first, the importing order is important.
    let smtpServerKeyMap = this._importSmtpServers(
      this._branchPrefsMap.get(SMTP_SERVER),
      this._collectPrefsToObject(this._branchPrefsMap.get(MAIL_SMTP))
        .defaultserver
    );

    // mail.identity.idN.smtpServer depends on transformed smtp server key.
    let identityKeyMap = this._importIdentities(
      this._branchPrefsMap.get(MAIL_IDENTITY),
      smtpServerKeyMap
    );
    let imAccountKeyMap = await this._importIMAccounts(
      this._branchPrefsMap.get(IM_ACCOUNT)
    );

    // mail.server.serverN.imAccount depends on transformed im account key.
    let incomingServerKeyMap = await this._importIncomingServers(
      this._branchPrefsMap.get(MAIL_SERVER),
      imAccountKeyMap
    );

    // mail.account.accountN.{identities, server} depends on previous steps.
    let accountManager = this._collectPrefsToObject(
      this._branchPrefsMap.get(ACCOUNT_MANAGER)
    );
    this._importAccounts(
      this._branchPrefsMap.get(MAIL_ACCOUNT),
      accountManager.accounts,
      accountManager.defaultaccount,
      identityKeyMap,
      incomingServerKeyMap
    );

    await this._importMailMessages(incomingServerKeyMap);
  }

  /**
   * Collect an array of prefs to an object.
   * @param {PrefItem[]} prefs - An array of prefs.
   * @returns {Object} An object mapping pref name to pref value.
   */
  _collectPrefsToObject(prefs) {
    let obj = {};
    for (let [, name, value] of prefs) {
      obj[name] = value;
    }
    return obj;
  }

  /**
   * Import SMTP servers.
   * @param {PrefItem[]} prefs - All source prefs in the SMTP_SERVER branch.
   * @param {string} sourceDefaultServer - The value of mail.smtp.defaultserver
   *   in the source profile.
   * @returns {smtpServerKeyMap} A map from source server key to new server key.
   */
  _importSmtpServers(prefs, sourceDefaultServer) {
    let smtpServerKeyMap = new Map();
    let branch = Services.prefs.getBranch(SMTP_SERVER);
    for (let [type, name, value] of prefs) {
      let key = name.split(".")[0];
      let newServerKey = smtpServerKeyMap.get(key);
      if (!newServerKey) {
        // For every smtp server, create a new one to avoid conflicts.
        let server = MailServices.smtp.createServer();
        newServerKey = server.key;
        smtpServerKeyMap.set(key, newServerKey);
        this._logger.debug(
          `Mapping SMTP server from ${key} to ${newServerKey}`
        );
      }

      let newName = `${newServerKey}${name.slice(key.length)}`;
      branch[`set${type}Pref`](newName, value);
    }

    // Set defaultserver if it doesn't already exist.
    let defaultServer = Services.prefs.getCharPref(
      "mail.smtp.defaultserver",
      ""
    );
    if (sourceDefaultServer && !defaultServer) {
      Services.prefs.setCharPref(
        "mail.smtp.defaultserver",
        smtpServerKeyMap.get(sourceDefaultServer)
      );
    }
    return smtpServerKeyMap;
  }

  /**
   * Import mail identites.
   * @param {PrefItem[]} prefs - All source prefs in the MAIL_IDENTITY branch.
   * @param {SmtpServerKeyMap} smtpServerKeyMap - A map from the source SMTP
   *   server key to new SMTP server key.
   * @returns {IdentityKeyMap} A map from the source identity key to new identity
   *   key.
   */
  _importIdentities(prefs, smtpServerKeyMap) {
    let identityKeyMap = new Map();
    let branch = Services.prefs.getBranch(MAIL_IDENTITY);
    for (let [type, name, value] of prefs) {
      let key = name.split(".")[0];
      let newIdentityKey = identityKeyMap.get(key);
      if (!newIdentityKey) {
        // For every identity, create a new one to avoid conflicts.
        let identity = MailServices.accounts.createIdentity();
        newIdentityKey = identity.key;
        identityKeyMap.set(key, newIdentityKey);
        this._logger.debug(`Mapping identity from ${key} to ${newIdentityKey}`);
      }

      let newName = `${newIdentityKey}${name.slice(key.length)}`;
      let newValue = value;
      if (name.endsWith(".smtpServer")) {
        newValue = smtpServerKeyMap.get(value) || newValue;
      }
      branch[`set${type}Pref`](newName, newValue);
    }
    return identityKeyMap;
  }

  /**
   * Import IM accounts.
   * @param {Array<[string, string, number|string|boolean]>} prefs - All source
   *   prefs in the IM_ACCOUNT branch.
   * @returns {IMAccountKeyMap} A map from the source account key to new account
   *   key.
   */
  async _importIMAccounts(prefs) {
    let imAccountKeyMap = new Map();
    let branch = Services.prefs.getBranch(IM_ACCOUNT);

    let lastKey = 1;
    function _getUniqueAccountKey() {
      let key = `account${lastKey++}`;
      if (Services.prefs.getCharPref(`messenger.account.${key}.name`, "")) {
        return _getUniqueAccountKey();
      }
      return key;
    }

    for (let [type, name, value] of prefs) {
      let key = name.split(".")[0];
      let newAccountKey = imAccountKeyMap.get(key);
      if (!newAccountKey) {
        // For every account, create a new one to avoid conflicts.
        newAccountKey = _getUniqueAccountKey();
        imAccountKeyMap.set(key, newAccountKey);
        this._logger.debug(
          `Mapping IM account from ${key} to ${newAccountKey}`
        );
      }

      let newName = `${newAccountKey}${name.slice(key.length)}`;
      branch[`set${type}Pref`](newName, value);
    }

    return imAccountKeyMap;
  }

  /**
   * Import incoming servers.
   * @param {PrefItem[]} prefs - All source prefs in the MAIL_SERVER branch.
   * @param {IMAccountKeyMap} imAccountKeyMap - A map from the source account
   *   key to new account key.
   * @returns {IncomingServerKeyMap} A map from the source server key to new
   *   server key.
   */
  async _importIncomingServers(prefs, imAccountKeyMap) {
    let incomingServerKeyMap = new Map();
    let branch = Services.prefs.getBranch(MAIL_SERVER);

    let lastKey = 1;
    function _getUniqueIncomingServerKey() {
      let key = `server${lastKey++}`;
      if (branch.getCharPref(`${key}.type`, "")) {
        return _getUniqueIncomingServerKey();
      }
      return key;
    }

    for (let [type, name, value] of prefs) {
      let key = name.split(".")[0];
      let newServerKey = incomingServerKeyMap.get(key);
      if (!newServerKey) {
        // For every incoming server, create a new one to avoid conflicts.
        newServerKey = _getUniqueIncomingServerKey();
        incomingServerKeyMap.set(key, newServerKey);
        this._logger.debug(`Mapping server from ${key} to ${newServerKey}`);
      }

      let newName = `${newServerKey}${name.slice(key.length)}`;
      let newValue = value;
      if (newName.endsWith(".imAccount")) {
        newValue = imAccountKeyMap.get(value);
      }
      branch[`set${type}Pref`](newName, newValue || value);
    }
    return incomingServerKeyMap;
  }

  /**
   * Import mail accounts.
   * @param {PrefItem[]} prefs - All source prefs in the MAIL_ACCOUNT branch.
   * @param {string} sourceAccounts - The value of mail.accountmanager.accounts
   *   in the source profile.
   * @param {string} sourceDefaultAccount - The value of
   *   mail.accountmanager.defaultaccount in the source profile.
   * @param {IdentityKeyMap} identityKeyMap - A map from the source identity key
   *   to new identity key.
   * @param {IncomingServerKeyMap} incomingServerKeyMap - A map from the source
   *   server key to new server key.
   */
  _importAccounts(
    prefs,
    sourceAccounts,
    sourceDefaultAccount,
    identityKeyMap,
    incomingServerKeyMap
  ) {
    let accountKeyMap = new Map();
    let branch = Services.prefs.getBranch(MAIL_ACCOUNT);
    for (let [type, name, value] of prefs) {
      let key = name.split(".")[0];
      if (key == "lastKey") {
        continue;
      }
      let newAccountKey = accountKeyMap.get(key);
      if (!newAccountKey) {
        // For every account, create a new one to avoid conflicts.
        newAccountKey = MailServices.accounts.getUniqueAccountKey();
        accountKeyMap.set(key, newAccountKey);
      }

      let newName = `${newAccountKey}${name.slice(key.length)}`;
      let newValue = value;
      if (name.endsWith(".identities")) {
        newValue = identityKeyMap.get(value);
      } else if (name.endsWith(".server")) {
        newValue = incomingServerKeyMap.get(value);
      }
      branch[`set${type}Pref`](newName, newValue || value);
    }

    // Append newly create accounts to mail.accountmanager.accounts.
    let accounts = Services.prefs
      .getCharPref("mail.accountmanager.accounts", "")
      .split(",");
    if (accounts.length == 1 && accounts[0] == "") {
      accounts.length = 0;
    }
    if (sourceAccounts) {
      for (let sourceAccountKey of sourceAccounts.split(",")) {
        accounts.push(accountKeyMap.get(sourceAccountKey));
      }
      Services.prefs.setCharPref(
        "mail.accountmanager.accounts",
        accounts.join(",")
      );
    }

    // Set defaultaccount if it doesn't already exist.
    let defaultAccount = Services.prefs.getCharPref(
      "mail.accountmanager.defaultaccount",
      ""
    );
    if (sourceDefaultAccount && !defaultAccount) {
      Services.prefs.setCharPref(
        "mail.accountmanager.defaultaccount",
        accountKeyMap.get(sourceDefaultAccount)
      );
    }
  }

  /**
   * Copy mail folders from this._sourceProfileDir to the current profile dir.
   * @param {PrefKeyMap} incomingServerKeyMap - A map from the source server key
   *   to new server key.
   */
  async _importMailMessages(incomingServerKeyMap) {
    for (let key of incomingServerKeyMap.values()) {
      let branch = Services.prefs.getBranch(`${MAIL_SERVER}${key}.`);
      if (!branch) {
        continue;
      }
      let type = branch.getCharPref("type", "");
      let hostname = branch.getCharPref("hostname", "");
      if (!type || !hostname) {
        continue;
      }

      // Use .directory-rel instead of .directory because .directory is an
      // absolute path which may not exists.
      let directoryRel = branch.getCharPref("directory-rel", "");
      if (!directoryRel.startsWith("[ProfD]")) {
        continue;
      }
      directoryRel = directoryRel.slice("[ProfD]".length);

      let targetDir = Services.dirsvc.get("ProfD", Ci.nsIFile);
      if (type == "imap") {
        targetDir.append("ImapMail");
      } else if (type == "nntp") {
        targetDir.append("News");
      } else if (["none", "pop3", "rss"].includes(type)) {
        targetDir.append("Mail");
      } else {
        continue;
      }

      // Use the hostname as mail folder name and ensure it's unique.
      targetDir.append(hostname);
      targetDir.createUnique(Ci.nsIFile.DIRECTORY_TYPE, 0o755);

      let sourceDir = this._sourceProfileDir.clone();
      for (let part of directoryRel.split("/")) {
        sourceDir.append(part);
      }
      if (sourceDir.exists()) {
        this._recursivelyCopyMsgFolder(sourceDir, targetDir);
      }
      branch.setCharPref("directory", targetDir.path);
      // .directory-rel may be outdated, it will be created when first needed.
      branch.clearUserPref("directory-rel");

      if (type == "nntp") {
        // Use .file-rel instead of .file because .file is an absolute path
        // which may not exists.
        let fileRel = branch.getCharPref("newsrc.file-rel", "");
        if (!fileRel.startsWith("[ProfD]")) {
          continue;
        }
        fileRel = fileRel.slice("[ProfD]".length);
        let sourceNewsrc = this._sourceProfileDir.clone();
        for (let part of fileRel.split("/")) {
          sourceNewsrc.append(part);
        }
        let targetNewsrc = Services.dirsvc.get("ProfD", Ci.nsIFile);
        targetNewsrc.append("News");
        targetNewsrc.append(`newsrc-${hostname}`);
        targetNewsrc.createUnique(Ci.nsIFile.NORMAL_FILE_TYPE, 0o644);
        this._logger.debug(
          `Copying ${sourceNewsrc.path} to ${targetNewsrc.path}`
        );
        sourceNewsrc.copyTo(targetNewsrc.parent, targetNewsrc.leafName);
        branch.setCharPref("newsrc.file", targetNewsrc.path);
        // .file-rel may be outdated, it will be created when first needed.
        branch.clearUserPref("newsrc.file-rel");
      }
    }
  }

  /**
   * Copy a source msg folder to a destination.
   * @param {nsIFile} sourceDir - The source msg folder location.
   * @param {nsIFile} targetDir - The target msg folder location.
   */
  _recursivelyCopyMsgFolder(sourceDir, targetDir) {
    this._logger.debug(`Copying ${sourceDir.path} to ${targetDir.path}`);

    // Copy the whole sourceDir.
    if (this._items.mailMessages) {
      // Remove the folder so that nsIFile.copyTo doesn't copy into targetDir.
      targetDir.remove(false);
      sourceDir.copyTo(targetDir.parent, targetDir.leafName);
      return;
    }

    // Copy only the folder structure, databases and filter rules. Ignore the
    // messages themselves.
    for (let entry of sourceDir.directoryEntries) {
      if (entry.isDirectory()) {
        let newFolder = targetDir.clone();
        newFolder.append(entry.leafName);
        newFolder.create(Ci.nsIFile.DIRECTORY_TYPE, 0o755);
        this._recursivelyCopyMsgFolder(entry, newFolder);
      } else {
        let leafName = entry.leafName;
        let extName = leafName.slice(leafName.lastIndexOf(".") + 1);
        if (extName != leafName && ["msf", "dat"].includes(extName)) {
          entry.copyTo(targetDir, leafName);
        }
      }
    }
  }

  /**
   * Import msg folders from this._sourceProfileDir into the Local Folders of
   * the current profile.
   */
  _importMailMessagesToLocal() {
    // Make sure Local Folders exist first.
    let localServer;
    try {
      localServer = MailServices.accounts.localFoldersServer;
    } catch (e) {
      MailServices.accounts.createLocalMailAccount();
      localServer = MailServices.accounts.localFoldersServer;
    }
    let localRootDir = localServer.rootMsgFolder.filePath;

    // Import ImapMail folders.
    for (let name of ["ImapMail", "News", "Mail"]) {
      let sourceDir = this._sourceProfileDir.clone();
      sourceDir.append(name);

      for (let entry of sourceDir.directoryEntries) {
        if (entry.isDirectory()) {
          if (name == "Mail" && entry.leafName == "Feeds") {
            continue;
          }
          let targetDir = localRootDir.clone();
          let folderName = localServer.rootMsgFolder.generateUniqueSubfolderName(
            entry.leafName,
            null
          );
          localServer.rootMsgFolder.createSubfolder(folderName, null);
          targetDir.append(folderName + ".sbd");
          targetDir.create(Ci.nsIFile.DIRECTORY_TYPE, 0o755);
          this._recursivelyCopyMsgFolder(entry, targetDir);
        }
      }
    }
  }

  /**
   * Import logins.json and key4.db.
   */
  _importPasswords() {
    let sourceLoginsJson = this._sourceProfileDir.clone();
    sourceLoginsJson.append("logins.json");
    let sourceKeyDb = this._sourceProfileDir.clone();
    sourceKeyDb.append("key4.db");
    let targetLoginsJson = Services.dirsvc.get("ProfD", Ci.nsIFile);
    targetLoginsJson.append("logins.json");

    if (
      sourceLoginsJson.exists() &&
      sourceKeyDb.exists() &&
      !targetLoginsJson.exists()
    ) {
      // Only copy if logins.json doesn't exist in the current profile.
      sourceLoginsJson.copyTo(targetLoginsJson.parent, "");
      sourceKeyDb.copyTo(targetLoginsJson.parent, "");
    }
  }

  /**
   * Import a pref from source only when this pref has no user value in the
   * current profile.
   * @param {PrefItem[]} prefs - All source prefs to try to import.
   */
  _importOtherPrefs(prefs) {
    for (let [type, name, value] of prefs) {
      if (!Services.prefs.prefHasUserValue(name)) {
        Services.prefs[`set${type}Pref`](name, value);
      }
    }
  }

  /**
   * Import address books.
   * @param {PrefItem[]} prefs - All source prefs in the ADDRESS_BOOK branch.
   * @param {Object} ldapAutoComplete - Pref values of LDAP_AUTO_COMPLETE branch.
   * @param {boolean} ldapAutoComplete.useDirectory
   * @param {string} ldapAutoComplete.directoryServer
   */
  _importAddressBooks(prefs, ldapAutoComplete) {
    let keyMap = new Map();
    let branch = Services.prefs.getBranch(ADDRESS_BOOK);
    for (let [type, name, value] of prefs) {
      let key = name.split(".")[0];
      if (["pab", "history"].includes(key)) {
        continue;
      }
      let newKey = keyMap.get(key);
      if (!newKey) {
        // For every address book, create a new one to avoid conflicts.
        let uniqueCount = 0;
        newKey = key;
        while (true) {
          if (!branch.getCharPref(`${newKey}.filename`, "")) {
            break;
          }
          newKey = `${key}${++uniqueCount}`;
        }
        keyMap.set(key, newKey);
      }

      let newName = `${newKey}${name.slice(key.length)}`;
      branch[`set${type}Pref`](newName, value);
    }

    // Transform the value of ldap_2.autoComplete.directoryServer if needed.
    if (
      ldapAutoComplete.useDirectory &&
      ldapAutoComplete.directoryServer &&
      !Services.prefs.getBoolPref(`${LDAP_AUTO_COMPLETE}useDirectory`, false)
    ) {
      let key = ldapAutoComplete.directoryServer.split("/").slice(-1)[0];
      let newKey = keyMap.get(key);
      if (newKey) {
        Services.prefs.setBoolPref(`${LDAP_AUTO_COMPLETE}useDirectory`, true);
        Services.prefs.setCharPref(
          `${LDAP_AUTO_COMPLETE}directoryServer`,
          `ldap_2.servers.${newKey}`
        );
      }
    }

    this._copyAddressBookDatabases(keyMap);
  }

  /**
   * Copy sqlite files from this._sourceProfileDir to the current profile dir.
   * @param {Map<string, string>} keyMap - A map from the source address
   *   book key to new address book key.
   */
  _copyAddressBookDatabases(keyMap) {
    // Copy user created address books.
    for (let key of keyMap.values()) {
      let branch = Services.prefs.getBranch(`${ADDRESS_BOOK}${key}.`);
      let filename = branch.getCharPref("filename", "");
      if (!filename) {
        continue;
      }
      let sourceFile = this._sourceProfileDir.clone();
      sourceFile.append(filename);
      if (!sourceFile.exists()) {
        this._logger.debug(
          `Ignoring non-existing address boook file ${sourceFile.path}`
        );
        continue;
      }

      let targetFile = Services.dirsvc.get("ProfD", Ci.nsIFile);
      targetFile.append(sourceFile.leafName);
      targetFile.createUnique(Ci.nsIFile.NORMAL_FILE_TYPE, 0o644);
      this._logger.debug(`Copying ${sourceFile.path} to ${targetFile.path}`);
      sourceFile.copyTo(targetFile.parent, targetFile.leafName);

      branch.setCharPref("filename", targetFile.leafName);
    }

    // Copy or import Personal Address Book.
    this._importAddressBookDatabase("abook.sqlite");
    // Copy or import Collected Addresses.
    this._importAddressBookDatabase("history.sqlite");
  }

  /**
   * Copy a sqlite file from this._sourceProfileDir to the current profile dir.
   * @param {string} filename - The name of the sqlite file.
   */
  _importAddressBookDatabase(filename) {
    let sourceFile = this._sourceProfileDir.clone();
    sourceFile.append(filename);
    let targetFile = Services.dirsvc.get("ProfD", Ci.nsIFile);
    targetFile.append(filename);

    if (!sourceFile.exists()) {
      return;
    }

    if (!targetFile.exists()) {
      sourceFile.copyTo(targetFile.parent, "");
      return;
    }

    let dirId = MailServices.ab.newAddressBook(
      "tmp",
      "",
      Ci.nsIAbManager.JS_DIRECTORY_TYPE
    );
    let tmpDirectory = MailServices.ab.getDirectoryFromId(dirId);
    sourceFile.copyTo(targetFile.parent, tmpDirectory.fileName);

    let targetDirectory = MailServices.ab.getDirectory(
      `jsaddrbook://${filename}`
    );
    for (let card of tmpDirectory.childCards) {
      targetDirectory.addCard(card);
    }

    MailServices.ab.deleteAddressBook(tmpDirectory.URI);
  }

  /**
   * Import calendars.
   *
   * For storage calendars, we need to import everything from the source
   * local.sqlite to the target local.sqlite, which is not implemented yet, see
   * bug 1719582.
   * @param {PrefItem[]} prefs - All source prefs in the CALENDAR branch.
   */
  _importCalendars(prefs) {
    let branch = Services.prefs.getBranch(CALENDAR);
    for (let [type, name, value] of prefs) {
      branch[`set${type}Pref`](name, value);
    }
  }
}
