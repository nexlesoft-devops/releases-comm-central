# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Import

## Header

import-from-app = Import from Application

import-from-app-desc = Choose to import Accounts, Address Books, Calendars, and other data from:

import-address-book = Import Address Book File

import-calendar = Import Calendar File

## Buttons

button-cancel = Cancel

button-back = Back

button-continue = Continue

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = Import from { $app }

profiles-pane-desc = Choose the location from which to import

profile-file-picker-dir = Select a profile folder

profile-file-picker-zip = Select a zip file (smaller than 2GB)

items-pane-title = Select what to import

items-pane-desc = Import from

items-pane-checkbox-accounts = Accounts and Settings

items-pane-checkbox-address-books = Address Books

items-pane-checkbox-calendars = Calendars

items-pane-checkbox-mail-messages = Mail Messages

## Import dialog

progress-pane-title = Importing

progress-pane-restart-desc = Restart to finish importing.

error-pane-title = Error

error-message-zip-file-too-big = The selected zip file is larger than 2GB. Please extract it first, then import from the extracted folder instead.

error-message-extract-zip-file-failed = Failed to extract the zip file. Please extract it manually, then import from the extracted folder instead.

error-message-failed = Import failed unexpectedly, more information may be available in the Error Console.
